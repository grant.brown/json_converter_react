import React, { Component } from 'react';
import { HashRouter, Route, Switch} from 'react-router-dom';

import './Main.css';

import Transformer from './Transformer/Transformer';
import FAQs from './FAQs/FAQs';
import About from './About/About';


const Main = () => (
    <main>
        <Switch>
         <Route  path="/faqs" component={FAQs}/>
         <Route  path="/about" component={About}/>
         <Route  path="/" component={Transformer}/>
        </Switch>
    </main>
  )

export default Main

