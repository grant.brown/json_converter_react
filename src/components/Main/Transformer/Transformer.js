import React, { Component } from 'react'
import Tree from './Tree/Tree';
import './Transformer.css';

export default class Transformer extends Component {
    render() {
        return (
            <div className="transformer-content">
                <Tree></Tree>
            </div>
        )
    }
}
