import React, { Component } from 'react';
import SortableTree, { changeNodeAtPath, toggleExpandedForAll } from 'react-sortable-tree';
import convert from 'xml-js';
import Button from '@material-ui/core/Button';
import SplitButton from '../SplitButton/SplitButton';
import SimpleModal from '../SimpleModal/SimpleModal';
import axios from 'axios';

import sampleJSON from '../../../../utilities/sample.json';
import sampleXML from '../../../../utilities/example.xml';

import 'react-sortable-tree/style.css'; // This only needs to be imported once in your app
import './Tree.css';


export default class Tree extends Component {
  constructor(props) {
    super(props);

    this.state = {
      treeInput: [

        ],
      treeOutput: [

        ],
      uniqueID: 'N/A',
    };
    this.loadSampleJSON = this.loadSampleJSON.bind(this);
    this.saveSampleJSON = this.saveSampleJSON.bind(this);
    this.clearPanel = this.clearPanel.bind(this);
  }

JSONToObject(response,parentjson) {
  for (var key in response) {
    let json={};
    if (response.hasOwnProperty(key)) {                 
        json.title=key;
        if(typeof response[key]=='object' && response[key] !== null){
          json.children=[];            
          this.JSONToObject(response[key],json.children);
        }else{
          json.subtitle=response[key];
        }
    }
    parentjson.push(json);
  }
  
  this.setState({treeInput: parentjson});
}

backToJSON(data, savedObject) {
  for(let i = 0; i < data.length; i++) {
    let json={}
    let key = data[i].title
    if (data[i].children !== undefined) {
      json[key] = {};
      this.backToJSON(data[i].children,json[key]);
    } else {
      json[key] = data[i].subtitle;
    }
  savedObject = Object.assign(savedObject, json);
  }
  return savedObject;
}

loadSampleJSON() {
  this.clearPanel('treeInput');
  setTimeout(() =>{
    this.JSONToObject(sampleJSON, this.state.treeInput)
  }, 1)
  
}

saveSampleJSON() {
  let result = this.backToJSON(this.state.treeOutput, {});
  let uniqueID = this.generateID();
  this.setState({uniqueID: uniqueID});
  let today = new Date();
  let date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
  let time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
  let dateTime = date+' '+time;
  console.log(result, uniqueID);
  axios.get('http://localhost:3154/', {
    params: {
      id: uniqueID,
      body: result,
      createdAt: dateTime 
    }
  }).then(function (response) {
    console.log(response);
  })
  .catch(function (error) {
    console.log(error);
  })
}

JSONToXML() {
  let y = sampleXML.toString();
  console.log(y);
  let x = convert.xml2json(sampleXML, {compact: true, spaces: 4});
  console.log(x);
}


toggleTree =  (expanded, panel) => {
  
  this.setState( state => ({
    [panel]: toggleExpandedForAll({
      treeData: state[panel],
      expanded,
    }),
  }));
};


clearPanel (panel) {
  console.log(this.state[panel]);
  this.setState({[panel]: []});
  console.log(this.state[panel]);
}

generateID() {
  // Math.random should be unique because of its seeding algorithm.
  // Convert it to base 36 (numbers + letters), and grab the first 9 characters
  // after the decimal.
  return '_' + Math.random().toString(36).substr(2, 9);

}

render() {
  console.log("This is the current input ", this.state.treeInput);
  console.log("This is the current output ", this.state.treeOutput);
  var isEmpty = true;
  if (this.state.treeOutput.length > 0) {
    isEmpty = false;
  }
  console.log(isEmpty);
  const getNodeKey = ({ treeIndex }) => treeIndex;
  const externalNodeType = 'yourNodeType';
  return (
    <div id="material-overrides" className="tree-container">
      <div className="panel left-panel">
          <h1 className="panel-heading">Input</h1>
          <SplitButton>Load JSON</SplitButton>
          <div className="button-container">
            {/* <Button variant="contained" color="secondary" size="large" onClick={this.loadSampleJSON}>Load JSON</Button> */}
            <Button className="button-regular" disabled="true" variant="contained" color="secondary" size="large" onClick={this.JSONToXML}>Load XML</Button>
            <Button className="button-regular" variant="contained" color="secondary" size="large" onClick={() => this.toggleTree(true, 'treeInput')}>Expand All</Button>
            <Button className="button-regular" variant="contained" color="secondary" size="large" onClick={() => this.toggleTree(false, 'treeInput')}>Collapse All</Button>
            <Button className="button-regular" variant="contained" color="secondary" size="large" onClick={(() => this.clearPanel('treeInput'))}>Clear Panel</Button>
          </div>
        <SortableTree
          treeData={this.state.treeInput}
          onChange={treeInput => this.setState({ treeInput })}
          dndType={externalNodeType}
          shouldCopyOnOutsideDrop={true}
          generateNodeProps={({ node, path }) => ({
            title: (
              <input
                value={node.title}
                onChange={event => {
                  const title = event.target.value;
                  console.log(title);
                  this.setState(state => ({
                    
                    treeInput: changeNodeAtPath({
                      treeData: state.treeInput,
                      path,
                      getNodeKey,
                      newNode: { ...node, title },
                    }),
                  }));
                }}
              />
            ),
          })}
        />
      </div>

      <div className="panel right-panel">
      <h1 className="panel-heading">Output</h1>
          <div className="button-container">
            <SimpleModal disabled={isEmpty} uniqueID={this.state.uniqueID} saveSampleJSON={this.saveSampleJSON.bind(this)}></SimpleModal>
            <Button className="button-regular" variant="contained" color="primary"  size="large" onClick={() => this.toggleTree(true, 'treeOutput')}>Expand All</Button>
            <Button className="button-regular" variant="contained" color="primary"  size="large" onClick={() => this.toggleTree(false, 'treeOutput')}>Collapse All</Button>
            <Button className="button-regular" variant="contained" color="primary"  size="large" onClick={() => this.clearPanel('treeOutput')}>Clear Panel</Button>
          </div>

        <SortableTree
          treeData={this.state.treeOutput}
          onChange={treeOutput => this.setState({ treeOutput })}
          dndType={externalNodeType}
          shouldCopyOnOutsideDrop={false}
          generateNodeProps={({ node, path }) => ({
            title: (
              <input
                value={node.title}
                onChange={event => {
                  const title = event.target.value;
                  console.log(title);
                  this.setState(state => ({
                    
                    treeOutput: changeNodeAtPath({
                      treeData: state.treeOutput,
                      path,
                      getNodeKey,
                      newNode: { ...node, title },
                    }),
                  }));
                }}
              />
            ),
          })}
        />
      </div>

    </div>
  );
}
}

