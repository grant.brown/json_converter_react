import React, { Component } from 'react';
import './Sidebar.css';
import {Link} from 'react-router-dom';

export default class Sidebar extends Component {
    render() {
        return (
            <div className="sidebar">
                <Link className="sidebar-link" to='/'><i className="fas fa-cog"></i> Transform</Link>
                <Link className="sidebar-link" to='/faqs'><i className="far fa-question-circle"></i> FAQs</Link>
                <Link className="sidebar-link" to='/about'><i className="fas fa-info-circle"></i> About</Link>
            </div>
        )
    }
}
