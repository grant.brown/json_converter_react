import React from 'react';
import './App.css';
import 'react-sortable-tree/style.css';

import Header from './components/Header/Header';
import Sidebar from './components/Sidebar/Sidebar';
import Main from './components/Main/Main';

function App() {
  return (
    <div className="App">
      <Header></Header>
      <div className="main-container">
        <Sidebar></Sidebar>
        <Main></Main>
      </div>
    </div>
  );
}

export default App;

