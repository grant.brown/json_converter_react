### json_converter_react

receive JSON / XML request, output JSON response with keys from the request based on channel(?) details.

## TODO High Priority

- save responses in database
- BUG: property gets overwritten when adding child to childless parent, even if it gets reverted
- user shoud be able to find JSON piece of data based on ID, GENERATE UNIQUE ID WHEN THE SERVICE IS LOADED'
- prevent user from creating duplicate keys


## TODO Low Priority

- clear load service (prevent cloning the data a million times)
- fix warning with fontawesome / cookies being rejected in Chrome Version 80
- write detailed notes for the next programmer who gets this project
- index.js:1375 Warning: Can't perform a React state update on an unmounted component. This is a no-op, but it indicates a memory leak in your application. To fix, cancel all subscriptions and asynchronous tasks in the componentWillUnmount method.
- cross browser compatibility?
- clean up unused stuff




## database
